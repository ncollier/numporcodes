#include "MLFEM.h"

#undef  __FUNCT__
#define __FUNCT__ "DetermineLevel"
PetscInt DetermineLevel(PetscInt i,PetscInt j,PetscInt Nl,PetscInt sp)
{
  PetscInt k,ilevel=-1,jlevel=-1,space=sp;
  for(k=0;k<Nl;k++){ 
    if(i % space == 0) { ilevel = k; break; }
    space /= 2;
  }
  space=sp;
  for(k=0;k<Nl;k++){ 
    if(j % space == 0) { jlevel = k; break; }
    space /= 2;
  }
  return MAX(ilevel,jlevel);
}

#undef  __FUNCT__
#define __FUNCT__ "GridCreate2D"
PetscErrorCode GridCreate2D(Grid *_grid,PetscInt Nl,PetscInt Nf,PetscReal refine_ratio)
{
  Grid grid;
  PetscInt i,j,dim=2;
  PetscFunctionBegin;
  PetscErrorCode ierr;

  // dimension grid
  PetscValidPointer(_grid,1);
  ierr = PetscNew(struct _n_grid,_grid);CHKERRQ(ierr);
  grid = *_grid;

  // setup the IGAs
  ierr = PetscMalloc1(Nl,IGA,&(grid->iga));CHKERRQ(ierr);
  PetscInt Ne = Nf;
  for(i=0;i<Nl;i++){
    IGA iga = grid->iga[i];
    ierr = IGACreate(PETSC_COMM_WORLD,&iga);CHKERRQ(ierr);
    ierr = IGASetDof(iga,1);CHKERRQ(ierr);
    ierr = IGASetDim(iga,dim);CHKERRQ(ierr);
    IGAAxis axis0;
    ierr = IGAGetAxis(iga,0,&axis0);CHKERRQ(ierr);
    ierr = IGAAxisSetPeriodic(axis0,PETSC_TRUE);CHKERRQ(ierr);
    ierr = IGAAxisSetDegree(axis0,1);CHKERRQ(ierr);
    ierr = IGAAxisInitUniform(axis0,Ne,0.0,1.0,0);CHKERRQ(ierr);
    IGAAxis axis1;
    ierr = IGAGetAxis(iga,1,&axis1);CHKERRQ(ierr);
    ierr = IGAAxisCopy(axis0,axis1);CHKERRQ(ierr);
    ierr = IGASetUp(iga);CHKERRQ(ierr);
    Ne /= refine_ratio;
    //ierr = IGAView(iga,PETSC_VIEWER_STDOUT_WORLD);
  }

  // determine grid size and spacing
  PetscInt N,sp=1;
  for(i=0;i<Nl-1;i++) sp *= 2;
  N = 2*sp+1;
  PetscPrintf(PETSC_COMM_WORLD,"number of blocks per dimension = %d\nspacing of anchors = %d\n",N,sp); 

  // setup the blocks
  PetscInt Nb = N*N;
  ierr = PetscMalloc1(Nb,GridBlock,&(grid->block));CHKERRQ(ierr);
  PetscPrintf(PETSC_COMM_WORLD,"structure of blocks:\n"); 
  for(j=0;j<N;j++){
    for(i=0;i<N;i++){
      printf(" %d ",DetermineLevel(i,j,Nl,sp));
    }
    printf("\n");
  }
  PetscFunctionReturn(0);
}

#undef  __FUNCT__
#define __FUNCT__ "GridSolveAnchor"
PetscErrorCode GridSolveAnchor(Grid grid)
{
  PetscFunctionBegin;


  PetscFunctionReturn(0);
}

#undef  __FUNCT__
#define __FUNCT__ "GridBlockSolveAnchor"
PetscErrorCode GridBlockSolveAnchor(GridBlock block)
{
  PetscFunctionBegin;


  PetscFunctionReturn(0);
}
