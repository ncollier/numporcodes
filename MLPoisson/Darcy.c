#include "petiga.h"

#undef  __FUNCT__
#define __FUNCT__ "Residual"
PetscErrorCode Residual(IGAPoint p,const PetscScalar *U,PetscScalar *R,void *ctx)
{    
  //AppCtx *user = (AppCtx *)ctx;

  const PetscReal *N0,(*N1)[2];
  IGAPointGetShapeFuns(p,0,(const PetscReal**)&N0);
  IGAPointGetShapeFuns(p,1,(const PetscReal**)&N1);

  PetscScalar u,u1[2];
  IGAPointFormValue(p,U,&u); 
  IGAPointFormGrad(p,U,&u1[0]);

  PetscScalar kappa = 1.+u1[0]*u1[0]+u1[1]*u1[1];
  PetscScalar f = sin(2*PETSC_PI*p->point[0])*sin(2*PETSC_PI*p->point[1]);

  PetscInt a,nen=p->nen;
  for (a=0; a<nen; a++) {
    PetscReal Na    = N0[a];
    PetscReal Na_x  = N1[a][0];
    PetscReal Na_y  = N1[a][1];
    R[a] = kappa*(u1[0]*Na_x+u1[1]*Na_y) - Na*f;
  }
  return 0;
}


#undef  __FUNCT__
#define __FUNCT__ "Jacobian"
PetscErrorCode Jacobian(IGAPoint p,const PetscScalar *U,PetscScalar *J,void *ctx)
{    
  //AppCtx *user = (AppCtx *)ctx;

  const PetscReal *N0,(*N1)[2];
  IGAPointGetShapeFuns(p,0,(const PetscReal**)&N0);
  IGAPointGetShapeFuns(p,1,(const PetscReal**)&N1);

  PetscScalar u,u1[2];
  IGAPointFormValue(p,U,&u); 
  IGAPointFormGrad(p,U,&u1[0]);

  PetscScalar kappa = 1.+u1[0]*u1[0]+u1[1]*u1[1];
  
  PetscInt a,b,nen=p->nen;
  for (a=0; a<nen; a++) {
    PetscReal Na_x  = N1[a][0];
    PetscReal Na_y  = N1[a][1];
    for (b=0; b<nen; b++) {
      PetscReal Nb_x  = N1[b][0];
      PetscReal Nb_y  = N1[b][1];
      J[a*nen+b] = kappa*(Na_x*Nb_x+Na_y*Nb_y) + 2.0*(Nb_x*u1[0]+Nb_y*u1[1])*(Na_x*u1[0]+Na_y*u1[1]);
    }
  }
  return 0;
}

#undef __FUNCT__
#define __FUNCT__ "main"
int main(int argc, char *argv[]) {

  PetscErrorCode ierr;
  ierr = PetscInitialize(&argc,&argv,0,0);CHKERRQ(ierr);

  PetscInt N = 20, p = 1, C = 0;
  ierr = PetscOptionsBegin(PETSC_COMM_WORLD,"","Darcy Options","IGA");CHKERRQ(ierr);
  ierr = PetscOptionsInt("-N","number of elements (along one dimension)",__FILE__,N,&N,PETSC_NULL);CHKERRQ(ierr);
  ierr = PetscOptionsInt("-p","polynomial order",__FILE__,p,&p,PETSC_NULL);CHKERRQ(ierr);
  ierr = PetscOptionsInt("-C","global continuity order",__FILE__,C,&C,PETSC_NULL);CHKERRQ(ierr);
  ierr = PetscOptionsEnd();CHKERRQ(ierr);
  
  IGA iga;
  ierr = IGACreate(PETSC_COMM_WORLD,&iga);CHKERRQ(ierr);
  ierr = IGASetDim(iga,2);CHKERRQ(ierr);
  ierr = IGASetDof(iga,1);CHKERRQ(ierr);
  
  IGAAxis axis0;
  ierr = IGAGetAxis(iga,0,&axis0);CHKERRQ(ierr);
  ierr = IGAAxisSetPeriodic(axis0,PETSC_TRUE);CHKERRQ(ierr);
  ierr = IGAAxisSetDegree(axis0,p);CHKERRQ(ierr);
  ierr = IGAAxisInitUniform(axis0,N,0.0,1.0,C);CHKERRQ(ierr);
  IGAAxis axis1;
  ierr = IGAGetAxis(iga,1,&axis1);CHKERRQ(ierr);
  ierr = IGAAxisCopy(axis0,axis1);CHKERRQ(ierr);

  ierr = IGASetFromOptions(iga);CHKERRQ(ierr);
  ierr = IGASetUp(iga);CHKERRQ(ierr);

  Vec U;
  ierr = IGACreateVec(iga,&U);CHKERRQ(ierr);

  SNES snes;
  ierr = IGASetUserFunction(iga,Residual,PETSC_NULL);CHKERRQ(ierr);
  ierr = IGASetUserJacobian(iga,Jacobian,PETSC_NULL);CHKERRQ(ierr);
  ierr = IGACreateSNES(iga,&snes);CHKERRQ(ierr);
  ierr = SNESSetFromOptions(snes);CHKERRQ(ierr);
  
  ierr = SNESSolve(snes,PETSC_NULL,U);CHKERRQ(ierr);
  
  ierr = VecDestroy(&U);CHKERRQ(ierr);
  ierr = SNESDestroy(&snes);CHKERRQ(ierr);
  ierr = IGADestroy(&iga);CHKERRQ(ierr);
  ierr = PetscFinalize();CHKERRQ(ierr);
  return 0;
}
