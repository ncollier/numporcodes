#ifndef _MLFEM_
#define _MLFEM_
#include "petiga.h"

typedef struct _n_grid_block *GridBlock;
typedef struct _n_grid       *Grid;

struct _n_grid_block{
  PetscInt is_anchor,level;
  IGA iga;
  Mat M,K;
  Vec N;
  PetscReal corner[3];
};

struct _n_grid{
  PetscInt N[3]; // number of grid blocks in each direction
  GridBlock *block;
  IGA *iga;
};

PetscErrorCode GridCreate2D(Grid *grid,PetscInt Nl,PetscInt Nf,PetscReal refine_ratio);
PetscErrorCode GridSolveAnchor(Grid grid);

PetscErrorCode GridBlockSolveAnchor(GridBlock block);

#endif
