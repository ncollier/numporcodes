#include <deal.II/base/quadrature_lib.h>
#include <deal.II/base/logstream.h>
#include <deal.II/base/function.h>
#include <deal.II/base/utilities.h>
#include <deal.II/lac/block_vector.h>
#include <deal.II/lac/full_matrix.h>
#include <deal.II/lac/block_sparse_matrix.h>
#include <deal.II/lac/solver_cg.h>
#include <deal.II/lac/precondition.h>
#include <deal.II/lac/constraint_matrix.h>
#include <deal.II/grid/tria.h>
#include <deal.II/grid/grid_generator.h>
#include <deal.II/grid/tria_accessor.h>
#include <deal.II/grid/tria_iterator.h>
#include <deal.II/grid/tria_boundary_lib.h>
#include <deal.II/grid/grid_tools.h>
#include <deal.II/grid/grid_refinement.h>
#include <deal.II/dofs/dof_handler.h>
#include <deal.II/dofs/dof_renumbering.h>
#include <deal.II/dofs/dof_accessor.h>
#include <deal.II/dofs/dof_tools.h>
#include <deal.II/fe/fe_q.h>
#include <deal.II/fe/fe_system.h>
#include <deal.II/fe/fe_values.h>
#include <deal.II/numerics/vector_tools.h>
#include <deal.II/numerics/matrix_tools.h>
#include <deal.II/numerics/data_out.h>
#include <deal.II/numerics/error_estimator.h>
#include <deal.II/lac/sparse_direct.h>
#include <deal.II/lac/sparse_ilu.h>
#include <fstream>
#include <sstream>

#define TWOPI 6.283185307179586
#define SQ(A) (A)*(A)

// define some clunky global variables
int    MODE      =0; 
double ALPHA_STAR=0;
double EPS       =0;
double FCT       =0;
double CONTRAST  =0;

namespace Brinkman
{
  using namespace dealii;

  template <int dim> struct InnerPreconditioner;
  template <> struct InnerPreconditioner<2> { typedef SparseDirectUMFPACK type; };
  template <> struct InnerPreconditioner<3> { typedef SparseILU<double>   type; };

  template <int dim>
  class BrinkmanProblem
  {
  public:
    BrinkmanProblem (const unsigned int degree);
    void run ();
    BlockVector<double>       solution;
    Triangulation<dim>        triangulation;
    DoFHandler<dim>           dof_handler;
    const unsigned int        degree;

  private:
    void setup_dofs ();
    void assemble_system ();
    void solve ();
    void output_results (const unsigned int refinement_cycle) const;
    void refine_mesh ();
    void process_solution (const unsigned int cycle);

    FESystem<dim>             fe;
    ConstraintMatrix          constraints;
    BlockSparsityPattern      sparsity_pattern;
    BlockSparseMatrix<double> system_matrix;
    BlockVector<double>       system_rhs;
    std_cxx1x::shared_ptr<typename InnerPreconditioner<dim>::type> A_preconditioner;
  };

 // Define a function for the zero solution (yeah, I know its weird)

  template <int dim> class Solution : public Function<dim>
  {
  public:
    Solution () : Function<dim>(dim+1) {}
    virtual double value (const Point<dim>   &p,const unsigned int  component = 0) const;
    virtual void vector_value (const Point<dim> &p,Vector<double> &value) const;
  };
  template <int dim> double Solution<dim>::value (const Point<dim>  &p,const unsigned int component) const
  {
    Assert (component < this->n_components,ExcIndexRange (component, 0, this->n_components));
    return 0.0;
  }
  template <int dim> void Solution<dim>::vector_value (const Point<dim> &p,Vector<double> &values) const
  {
    for (unsigned int c=0; c<this->n_components; ++c) values(c) = Solution<dim>::value (p, c);
  }


  // Define a boundary function for setting velocity boundary conditions

  template <int dim> class BoundaryValues : public Function<dim>
  {
  public:
    BoundaryValues () : Function<dim>(dim+1) {}
    virtual double value (const Point<dim>   &p,const unsigned int  component = 0) const;
    virtual void vector_value (const Point<dim> &p,Vector<double> &value) const;
  };
  template <int dim> double BoundaryValues<dim>::value (const Point<dim>  &p,const unsigned int component) const
  {
    Assert (component < this->n_components,ExcIndexRange (component, 0, this->n_components));
    return 0.0;
  }
  template <int dim> void BoundaryValues<dim>::vector_value (const Point<dim> &p,Vector<double> &values) const
  {
    for (unsigned int c=0; c<this->n_components; ++c) values(c) = BoundaryValues<dim>::value (p, c);
  }
  

  // Define a RHS function for setting the forcing

  template <int dim>
  class RightHandSide : public Function<dim>
  {
  public:
    RightHandSide () : Function<dim>(dim+1) {}
    virtual double value (const Point<dim>   &p,const unsigned int component = 0) const;
    virtual void vector_value (const Point<dim> &p, Vector<double> &value) const;
  };
  template <int dim> double RightHandSide<dim>::value (const Point<dim>  &p,const unsigned int component) const
  {
    if (MODE == 0){
      if (component == 0) return 1.;
    }else{
      if (component <= 1) {
	double f = 0;
	for(int i=0;i<2;i++){
	  double r = sqrt(SQ(p[0]-(0.33+i*0.33))+SQ(p[1]-(0.33+i*0.33)));
	  r /= (0.33*0.5*0.9);
	  if(r>1) r = 1;
	  f += pow(-1,i)*SQ(1.-r*r);
	}
	return f;
      }
    }
    return 0;
  }
  template <int dim> void RightHandSide<dim>::vector_value (const Point<dim> &p,Vector<double>   &values) const
  {
    for (unsigned int c=0; c<this->n_components; ++c) values(c) = RightHandSide<dim>::value (p, c);
  }

  // Define the reciprocal permeability function 

  template <int dim> class Alpha : public Function<dim>
  {
  public:
    Alpha () : Function<dim>(dim+1) {}
    virtual double value      (const Point<dim> &p,const unsigned int component = 0) const;
    virtual void vector_value (const Point<dim> &p,Vector<double> &value) const;
  };
  template <int dim> double Alpha<dim>::value (const Point<dim>  &p,const unsigned int component) const
  {
    // double low = 0.5-0.25*sqrt(2.0);
    // double hi  = 0.5+0.25*sqrt(2.0);
    // if (MODE == 0){
    //   if( p[0] > low && p[1] > low && p[0] < hi && p[1] < hi) return EPS*EPS*CONTRAST; 
    //   return EPS*EPS;
    // }else if(MODE == 1){
    //   return ALPHA_STAR; 
    // }else{
    //   double x = (p[0]-((int)(p[0]/EPS))*EPS)/EPS;
    //   double y = (p[1]-((int)(p[1]/EPS))*EPS)/EPS;
    //   if( x > low && y > low && x < hi && y < hi) return CONTRAST; 
    //   return 1.0;
    // }

    double r0 = sqrt(0.5/PETSC_PI);
    if (MODE == 0){
      double r = sqrt(SQ(p[0]-0.5) + SQ(p[1]-0.5));
      if( r < r0 ) return EPS*EPS*CONTRAST; 
      return EPS*EPS;
    }else if(MODE == 1){
      return ALPHA_STAR; 
    }else{
      double x = (p[0]-((int)(p[0]/EPS))*EPS)/EPS;
      double y = (p[1]-((int)(p[1]/EPS))*EPS)/EPS;
      double r = sqrt(SQ(x-0.5)+SQ(y-0.5));
      if( r < r0 ) return EPS*EPS*CONTRAST; 
      return 1.0;
    }

  }
  template <int dim> void Alpha<dim>::vector_value (const Point<dim> &p,Vector<double> &values) const
  {
    for (unsigned int c=0; c<this->n_components; ++c) values(c) = Alpha<dim>::value (p, c);
  }


  // Setup preconditioner -- dont look at this part it is ugly

  template <class Matrix, class Preconditioner>
  class InverseMatrix : public Subscriptor
  {
  public:
    InverseMatrix (const Matrix &m,const Preconditioner &preconditioner);
    void vmult (Vector<double> &dst, const Vector<double> &src) const;
  private:
    const SmartPointer<const Matrix> matrix;
    const SmartPointer<const Preconditioner> preconditioner;
  };
  template <class Matrix, class Preconditioner>
  InverseMatrix<Matrix,Preconditioner>::InverseMatrix (const Matrix &m,
                                                       const Preconditioner &preconditioner)
    :
    matrix (&m),
    preconditioner (&preconditioner)
  {}
  template <class Matrix, class Preconditioner>
  void InverseMatrix<Matrix,Preconditioner>::vmult (Vector<double>       &dst,
                                                    const Vector<double> &src) const
  {
    SolverControl solver_control (src.size(), 1e-6*src.l2_norm());
    SolverCG<>    cg (solver_control);

    dst = 0;

    cg.solve (*matrix, dst, src, *preconditioner);
  }
  template <class Preconditioner>
  class SchurComplement : public Subscriptor
  {
  public:
    SchurComplement (const BlockSparseMatrix<double> &system_matrix,
		     const InverseMatrix<SparseMatrix<double>, Preconditioner> &A_inverse);
    void vmult (Vector<double>       &dst,
		const Vector<double> &src) const;
  private:
    const SmartPointer<const BlockSparseMatrix<double> > system_matrix;
    const SmartPointer<const InverseMatrix<SparseMatrix<double>, Preconditioner> > A_inverse;
    mutable Vector<double> tmp1, tmp2;
  };
  template <class Preconditioner>
  SchurComplement<Preconditioner>::
  SchurComplement (const BlockSparseMatrix<double> &system_matrix,
                   const InverseMatrix<SparseMatrix<double>,Preconditioner> &A_inverse)
    :
    system_matrix (&system_matrix),
    A_inverse (&A_inverse),
    tmp1 (system_matrix.block(0,0).m()),
    tmp2 (system_matrix.block(0,0).m())
  {}
  template <class Preconditioner>
  void SchurComplement<Preconditioner>::vmult (Vector<double>       &dst,
                                               const Vector<double> &src) const
  {
    system_matrix->block(0,1).vmult (tmp1, src);
    A_inverse->vmult (tmp2, tmp1);
    system_matrix->block(1,0).vmult (dst, tmp2);
  }


  // Brinkman problem functions

  template <int dim>
  BrinkmanProblem<dim>::BrinkmanProblem (const unsigned int degree)
    :
    degree (degree),
    triangulation (Triangulation<dim>::maximum_smoothing),
    fe (FE_Q<dim>(degree+1), dim,
	FE_Q<dim>(degree), 1),
    dof_handler (triangulation)
  {}

  template <int dim>
  void BrinkmanProblem<dim>::setup_dofs ()
  {
    A_preconditioner.reset ();
    system_matrix.clear ();

    dof_handler.distribute_dofs (fe);
    DoFRenumbering::Cuthill_McKee (dof_handler);

    std::vector<unsigned int> block_component (dim+1,0);
    block_component[dim] = 1;
    DoFRenumbering::component_wise (dof_handler, block_component);

    {
      constraints.clear ();
      // hanging nodes
      DoFTools::make_hanging_node_constraints (dof_handler,
                                               constraints);

      if(MODE == 0){
	// periodic boundary conditions
	DoFTools::make_periodicity_constraints (dof_handler,
						1,
						0,
						constraints);
	DoFTools::make_periodicity_constraints (dof_handler,
						1,
						1,
						constraints);
      }else{
	// mask the pressure, set velocity on bndry markers = 1
	std::vector<bool> component_mask (dim+1, true);
	component_mask[dim] = false;
	VectorTools::interpolate_boundary_values (dof_handler,
						  1,
						  BoundaryValues<dim>(),
						  constraints,
						  component_mask);
      }
    }

    constraints.close ();

    std::vector<unsigned int> dofs_per_block (2);
    DoFTools::count_dofs_per_block (dof_handler, dofs_per_block, block_component);
    const unsigned int n_u = dofs_per_block[0],
      n_p = dofs_per_block[1];

    std::cout << "   Number of active cells: "
              << triangulation.n_active_cells()
              << std::endl
              << "   Number of degrees of freedom: "
              << dof_handler.n_dofs()
              << " (" << n_u << '+' << n_p << ')'
              << std::endl;

    {
      BlockCompressedSimpleSparsityPattern csp (2,2);

      csp.block(0,0).reinit (n_u, n_u);
      csp.block(1,0).reinit (n_p, n_u);
      csp.block(0,1).reinit (n_u, n_p);
      csp.block(1,1).reinit (n_p, n_p);

      csp.collect_sizes();

      DoFTools::make_sparsity_pattern (dof_handler, csp, constraints, false);
      sparsity_pattern.copy_from (csp);
    }

    system_matrix.reinit (sparsity_pattern);

    solution.reinit (2);
    solution.block(0).reinit (n_u);
    solution.block(1).reinit (n_p);
    solution.collect_sizes ();

    system_rhs.reinit (2);
    system_rhs.block(0).reinit (n_u);
    system_rhs.block(1).reinit (n_p);
    system_rhs.collect_sizes ();
  }

  template <int dim>
  void BrinkmanProblem<dim>::assemble_system ()
  {
    system_matrix=0;
    system_rhs=0;

    QGauss<dim>   quadrature_formula(degree+2);

    FEValues<dim> fe_values (fe, quadrature_formula,
                             update_values    |
                             update_quadrature_points  |
                             update_JxW_values |
                             update_gradients);

    const unsigned int   dofs_per_cell   = fe.dofs_per_cell;

    const unsigned int   n_q_points      = quadrature_formula.size();

    FullMatrix<double>   local_matrix (dofs_per_cell, dofs_per_cell);
    Vector<double>       local_rhs (dofs_per_cell);

    std::vector<unsigned int> local_dof_indices (dofs_per_cell);

    const RightHandSide<dim>          right_hand_side;
    std::vector<Vector<double> >      rhs_values (n_q_points,
                                                  Vector<double>(dim+1));
    const Alpha<dim>                  alpha;
    
    const FEValuesExtractors::Vector velocities (0);
    const FEValuesExtractors::Scalar pressure (dim);

    std::vector<SymmetricTensor<2,dim> > symgrad_phi_u (dofs_per_cell);
    std::vector<double>                  div_phi_u   (dofs_per_cell);
    std::vector<double>                  phi_p       (dofs_per_cell);
    std::vector<Tensor<1,2,double> >     phi_u       (dofs_per_cell);

    typename DoFHandler<dim>::active_cell_iterator
      cell = dof_handler.begin_active(),
      endc = dof_handler.end();
    for (; cell!=endc; ++cell)
      {
        fe_values.reinit (cell);
        local_matrix = 0;
        local_rhs = 0;

        right_hand_side.vector_value_list(fe_values.get_quadrature_points(),
                                          rhs_values);

        for (unsigned int q=0; q<n_q_points; ++q)
          {
            for (unsigned int k=0; k<dofs_per_cell; ++k)
              {
                symgrad_phi_u[k] = fe_values[velocities].symmetric_gradient (k, q);
                div_phi_u[k]     = fe_values[velocities].divergence (k, q);
                phi_p[k]         = fe_values[pressure].value (k, q);
                phi_u[k]         = fe_values[velocities].value (k, q);
              }

            for (unsigned int i=0; i<dofs_per_cell; ++i)
              {
                for (unsigned int j=0; j<=i; ++j)
                  {
                    local_matrix(i,j) += (symgrad_phi_u[i] * symgrad_phi_u[j]
                                          - div_phi_u[i] * phi_p[j]
                                          - phi_p[i] * div_phi_u[j]
					  + alpha.value(fe_values.quadrature_point(q),0) * phi_u[i] * phi_u[j]
                                          + phi_p[i] * phi_p[j])
		      * fe_values.JxW(q);

                  }

                const unsigned int component_i =
                  fe.system_to_component_index(i).first;
                local_rhs(i) += fe_values.shape_value(i,q) *
		  rhs_values[q](component_i) *
		  fe_values.JxW(q);
              }
          }

        for (unsigned int i=0; i<dofs_per_cell; ++i)
          for (unsigned int j=i+1; j<dofs_per_cell; ++j)
            local_matrix(i,j) = local_matrix(j,i);

        cell->get_dof_indices (local_dof_indices);
        constraints.distribute_local_to_global (local_matrix, local_rhs,
                                                local_dof_indices,
                                                system_matrix, system_rhs);
      }

    std::cout << "   Computing preconditioner..." << std::endl << std::flush;

    A_preconditioner
      = std_cxx1x::shared_ptr<typename InnerPreconditioner<dim>::type>(new typename InnerPreconditioner<dim>::type());
    A_preconditioner->initialize (system_matrix.block(0,0),
                                  typename InnerPreconditioner<dim>::type::AdditionalData());

  }



  template <int dim>
  void BrinkmanProblem<dim>::solve ()
  {
    const InverseMatrix<SparseMatrix<double>,
			typename InnerPreconditioner<dim>::type>
    A_inverse (system_matrix.block(0,0), *A_preconditioner);
  Vector<double> tmp (solution.block(0).size());

  {
    Vector<double> schur_rhs (solution.block(1).size());
    A_inverse.vmult (tmp, system_rhs.block(0));
    system_matrix.block(1,0).vmult (schur_rhs, tmp);
    schur_rhs -= system_rhs.block(1);

    SchurComplement<typename InnerPreconditioner<dim>::type>
      schur_complement (system_matrix, A_inverse);

    SolverControl solver_control (solution.block(1).size(),
				  1e-6*schur_rhs.l2_norm());
    SolverCG<>    cg (solver_control);

    SparseILU<double> preconditioner;
    preconditioner.initialize (system_matrix.block(1,1),
			       SparseILU<double>::AdditionalData());

    InverseMatrix<SparseMatrix<double>,SparseILU<double> >
      m_inverse (system_matrix.block(1,1), preconditioner);

    cg.solve (schur_complement, solution.block(1), schur_rhs,
	      m_inverse);

    constraints.distribute (solution);

    std::cout << "  "
	      << solver_control.last_step()
	      << " outer CG Schur complement iterations for pressure"
	      << std::endl;
  }

    {
      system_matrix.block(0,1).vmult (tmp, solution.block(1));
      tmp *= -1;
      tmp += system_rhs.block(0);

      A_inverse.vmult (solution.block(0), tmp);

      constraints.distribute (solution);
    }
}

  template <int dim>
  void BrinkmanProblem<dim>::process_solution (const unsigned int refinement_cycle)
  {
    const ComponentSelectFunction<dim> mask_component (0, dim+1);

    const QTrapez<1>     q_trapez;
    const QIterated<dim> q_iterated (q_trapez, 5);

    Vector<float> difference_per_cell (triangulation.n_active_cells());
    VectorTools::integrate_difference (dof_handler,
				       solution,
				       Solution<dim>(),
				       difference_per_cell,
				       q_iterated,
				       VectorTools::L1_norm,
				       &mask_component);
    
    const double Mean = EPS*EPS*difference_per_cell.l1_norm();
    ALPHA_STAR = 1/Mean;
    std::cout<<"Permeability: "<<Mean<<std::endl;
    std::cout<<"Alpha: "<<ALPHA_STAR<<std::endl;
    
  }

template <int dim>
void
BrinkmanProblem<dim>::output_results (const unsigned int refinement_cycle)  const
{
  std::vector<std::string> solution_names (dim, "velocity");
  solution_names.push_back ("pressure");

  std::vector<DataComponentInterpretation::DataComponentInterpretation>
    data_component_interpretation
    (dim, DataComponentInterpretation::component_is_part_of_vector);
  data_component_interpretation
    .push_back (DataComponentInterpretation::component_is_scalar);

  DataOut<dim> data_out;
  data_out.attach_dof_handler (dof_handler);
  data_out.add_data_vector (solution, solution_names,
			    DataOut<dim>::type_dof_data,
			    data_component_interpretation);
  data_out.build_patches ();

  std::ostringstream filename;
  filename << "solution-"
           << Utilities::int_to_string (MODE, 1)
           << "-"
	   << Utilities::int_to_string (refinement_cycle, 2)
	   << ".vtk";

  std::ofstream output (filename.str().c_str());
  data_out.write_vtk (output);
}


template <int dim>
void
BrinkmanProblem<dim>::refine_mesh ()
{
  Vector<float> estimated_error_per_cell (triangulation.n_active_cells());

  std::vector<bool> component_mask (dim+1, false);
  component_mask[dim] = true;
  KellyErrorEstimator<dim>::estimate (dof_handler,
				      QGauss<dim-1>(degree+1),
				      typename FunctionMap<dim>::type(),
				      solution,
				      estimated_error_per_cell,
				      component_mask);

  GridRefinement::refine_and_coarsen_fixed_number (triangulation,
						   estimated_error_per_cell,
						   0.3, 0.0);
  triangulation.execute_coarsening_and_refinement ();
}


template <int dim>
void BrinkmanProblem<dim>::run ()
{
  {
    std::vector<unsigned int> subdivisions (dim, 1);

    const Point<dim> bottom_left = (dim == 2 ?
				    Point<dim>(0,0) :
				    Point<dim>(0,0,0));
    const Point<dim> top_right   = (dim == 2 ?
				    Point<dim>(1,1) :
				    Point<dim>(1,1,1));

    GridGenerator::subdivided_hyper_rectangle (triangulation,
					       subdivisions,
					       bottom_left,
					       top_right);
  }

  for (typename Triangulation<dim>::active_cell_iterator
	 cell = triangulation.begin_active();
       cell != triangulation.end(); ++cell)
    for (unsigned int f=0; f<GeometryInfo<dim>::faces_per_cell; ++f){
      if (cell->face(f)->center()[1] == 0 || cell->face(f)->center()[1] == 1 ||
	  cell->face(f)->center()[0] == 0 || cell->face(f)->center()[0] == 1 )
	cell->face(f)->set_all_boundary_indicators(1);
    }

  int num_refine = 6; 
  if(MODE == 2) num_refine = (int)(log10(EPS/FCT)/log10(0.5)) + 1;
  triangulation.refine_global (num_refine);

  int num_cycle = 1;
  if(MODE == 2) num_cycle = 4;

  for (unsigned int refinement_cycle = 0; refinement_cycle<num_cycle;
       ++refinement_cycle)
    {
      std::cout << "Refinement cycle " << refinement_cycle << std::endl;

      if (refinement_cycle > 0) refine_mesh ();

      setup_dofs ();

      std::cout << "   Assembling..." << std::endl << std::flush;
      assemble_system ();

      std::cout << "   Solving..." << std::flush;
      solve ();

      if(MODE == 0) process_solution (refinement_cycle); 
      output_results (refinement_cycle);

      std::cout << std::endl;
    }
}


// Corrector to the fine scale problem


template <int dim> class Corrector : public Function<dim>
{
public:
  Corrector (BrinkmanProblem<dim> *homo_problem,BrinkmanProblem<dim> *cell_problem) : Function<dim>(dim+1) 
  {
    _homo_problem = homo_problem;
    _cell_problem = cell_problem;
  }
  virtual double value (const Point<dim>   &p,const unsigned int  component = 0) const;
  virtual void vector_value (const Point<dim> &p,Vector<double> &value) const;
  BrinkmanProblem<dim> *_homo_problem,*_cell_problem;
};
template <int dim> double Corrector<dim>::value (const Point<dim>  &p,const unsigned int component) const
{
  Assert (component < this->n_components,ExcIndexRange (component, 0, this->n_components));


  return 0.0;
}
template <int dim> void Corrector<dim>::vector_value (const Point<dim> &p,Vector<double> &values) const
{
  VectorTools::point_value(_homo_problem->dof_handler,
  			   _homo_problem->solution,
   			   p,
   			   values);

  Point<dim> ps;
  ps[0] = (p[0]-((int)(p[0]/EPS))*EPS)/EPS;
  ps[1] = (p[1]-((int)(p[1]/EPS))*EPS)/EPS;

  Vector<double> tmp;
  VectorTools::point_value(_cell_problem->dof_handler,
  			   _cell_problem->solution,
   			   ps,
   			   tmp);
  tmp *= EPS*EPS*ALPHA_STAR;
  for(int i=0;i<3;i++) values[i] *= tmp[i];
}


}


int main (int argc, char *argv[])
{
  try
    {
      using namespace dealii;
      using namespace Brinkman;

      deallog.depth_console (0);

      EPS = 0.1; // pore size
      FCT = 5.0; // h is FCT times smaller than EPS
      CONTRAST = 1.0e6; 

      if(argc >= 2) EPS = atof(argv[1]);
      if(argc >= 3) CONTRAST = atof(argv[2]);

      std::cout << "\nSolving the cell problem...\n" << std::endl;

      MODE = 0;
      BrinkmanProblem<2> cell_problem(1);
      cell_problem.run ();

      std::cout << "\nSolving the homogenized problem...\n" << std::endl;

      MODE = 1;
      BrinkmanProblem<2> homo_problem(1);
      homo_problem.run ();

      std::cout << "\nSolving the fine problem...\n" << std::endl;

      MODE = 2;
      BrinkmanProblem<2> fine_problem(1);
      fine_problem.run ();

      const ComponentSelectFunction<2> mask_component (0,2,3);
      Vector<float> difference_per_cell (fine_problem.triangulation.n_active_cells());
      Corrector<2> corrector(&homo_problem,&cell_problem);
      VectorTools::integrate_difference (fine_problem.dof_handler,
					 fine_problem.solution,
					 corrector,
					 difference_per_cell,
					 QGauss<2>(3),
					 VectorTools::L2_norm,
					 &mask_component);
      double l2norm = difference_per_cell.l1_norm() ;

      // normalize by l2 of fine
      difference_per_cell *= 0;
      VectorTools::integrate_difference (fine_problem.dof_handler,
					 fine_problem.solution,
					 Solution<2>(),
					 difference_per_cell,
					 QGauss<2>(3),
					 VectorTools::L2_norm,
					 &mask_component);

      l2norm /= difference_per_cell.l1_norm();
      l2norm = sqrt(l2norm);

      std::cout << "\nL2 Norm of Error: " << l2norm << std::endl;

    }
  catch (std::exception &exc)
    {
      std::cerr << std::endl << std::endl
                << "----------------------------------------------------"
                << std::endl;
      std::cerr << "Exception on processing: " << std::endl
                << exc.what() << std::endl
                << "Aborting!" << std::endl
                << "----------------------------------------------------"
                << std::endl;

      return 1;
    }
  catch (...)
    {
      std::cerr << std::endl << std::endl
                << "----------------------------------------------------"
                << std::endl;
      std::cerr << "Unknown exception!" << std::endl
                << "Aborting!" << std::endl
                << "----------------------------------------------------"
                << std::endl;
      return 1;
    }

  return 0;
}
