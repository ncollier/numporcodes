#include "petiga.h"

PetscScalar TOL_GRADU = 1e-12, TOL_FO = 0.95;

typedef struct { 
  IGA iga;
  PetscScalar m,gamma,alpha,Cf,kappa_max,h2;
} AppCtx;

#undef __FUNCT__
#define __FUNCT__ "Forcing"
PetscReal Forcing(PetscScalar *x,PetscScalar t)
{
  PetscScalar r0 = 1000, r = sqrt(x[0]*x[0]+x[1]*x[1]);
  PetscScalar t0 = 5.*60.*0.5; t = fabs(t-t0);
  if(r>r0 || t>t0) return 1e-12;
  PetscScalar space = 1-r*r/r0/r0;
  PetscScalar time = 1-t*t/t0/t0;
  return 2e-4*time*time*space*space+1e-12;
}

#undef  __FUNCT__
#define __FUNCT__ "Diffusivity"
void Diffusivity(PetscScalar u,PetscScalar *u_x,PetscScalar z,PetscScalar *kappa,PetscScalar *kappa_u,AppCtx *user)
{
  PetscReal du = u_x[0]*u_x[0]+u_x[1]*u_x[1];
  PetscReal alpha = user->alpha, gamma = user->gamma, Cf = user->Cf;
  PetscBool ok = (u >= z && sqrt(du) > TOL_GRADU);
  if (kappa) {
    *kappa = 0;
    if(ok) *kappa = pow(u-z,alpha)*pow(du,0.5*(gamma-1.))/Cf;
  }
  if (kappa_u) {
    kappa_u[0] = 0;
    kappa_u[1] = 0;
    if(ok){
      kappa_u[0] = pow(u-z,alpha)*(gamma-1.)*pow(du,0.5*(gamma-1.)-1.)/Cf;
      kappa_u[1] = alpha*pow(u-z,alpha-1.)*pow(du,0.5*(gamma-1.))/Cf;
    }
  }
  return;
}

#undef  __FUNCT__
#define __FUNCT__ "Residual"
PetscErrorCode Residual(IGAPoint p,PetscReal dt,
                        PetscReal shift,const PetscScalar *V,
                        PetscReal t,const PetscScalar *U,
                        PetscScalar *R,void *ctx)
{
  AppCtx *user = (AppCtx *)ctx;

  PetscInt nen;
  IGAPointGetSizes(p,0,&nen,0);

  PetscScalar u_t,u,u_x[2],z;
  IGAPointFormValue(p,V,&u_t);
  IGAPointFormValue(p,U,&u);
  IGAPointFormGrad (p,U,&u_x[0]);
  IGAPointFormValue(p,p->property,&z);

  PetscScalar kappa,f;
  Diffusivity(u,u_x,z,&kappa,NULL,user);
  f=Forcing(p->point,t);

  user->kappa_max = PetscMax(kappa,user->kappa_max);

  const PetscReal *N0,(*N1)[2];
  IGAPointGetShapeFuns(p,0,(const PetscReal**)&N0);
  IGAPointGetShapeFuns(p,1,(const PetscReal**)&N1);

  PetscInt a;
  for (a=0; a<nen; a++) {
    PetscReal Na    = N0[a];
    PetscReal Na_x  = N1[a][0];
    PetscReal Na_y  = N1[a][1];
    R[a] = Na*u_t + kappa*(Na_x*u_x[0]+Na_y*u_x[1]) - Na*f;
  }
  return 0;
}

#undef  __FUNCT__
#define __FUNCT__ "Jacobian"
PetscErrorCode Jacobian(IGAPoint p,PetscReal dt,
			PetscReal shift,const PetscScalar *V,
			PetscReal t,const PetscScalar *U,
			PetscScalar *J,void *ctx)
{
 AppCtx *user = (AppCtx *)ctx;

  PetscInt nen;
  IGAPointGetSizes(p,0,&nen,0);

  PetscScalar u_t,u,u_x[2],z;
  IGAPointFormValue(p,V,&u_t);
  IGAPointFormValue(p,U,&u);
  IGAPointFormGrad (p,U,&u_x[0]);
  IGAPointFormValue(p,p->property,&z);

  PetscScalar kappa,kappa_u[2];
  Diffusivity(u,u_x,z,&kappa,kappa_u,user);

  const PetscReal *N0,(*N1)[2];
  IGAPointGetShapeFuns(p,0,(const PetscReal**)&N0);
  IGAPointGetShapeFuns(p,1,(const PetscReal**)&N1);

  PetscInt a,b;
  for (a=0; a<nen; a++) {
    PetscReal Na    = N0[a];
    PetscReal Na_x  = N1[a][0];
    PetscReal Na_y  = N1[a][1];
    for (b=0; b<nen; b++) {
      PetscReal Nb    = N0[b];
      PetscReal Nb_x  = N1[b][0];
      PetscReal Nb_y  = N1[b][1];
      J[b*nen+a]  = shift*Na*Nb;
      J[b*nen+a] += kappa*(Na_x*Nb_x+Na_y*Nb_y);
      J[b*nen+a] += kappa_u[0]*(Na_x*u_x[0]+Na_y*u_x[1]);
      J[b*nen+a] += kappa_u[1]*Nb;
    }
  }
  return 0;
}

#undef __FUNCT__
#define __FUNCT__ "OutputMonitor"
PetscErrorCode OutputMonitor(TS ts,PetscInt it_number,PetscReal c_time,Vec U,void *mctx)
{
  PetscFunctionBegin;
  PetscErrorCode ierr;
  AppCtx *user = (AppCtx *)mctx;
  char           filename[256];
  sprintf(filename,"./solution/dsw%d.dat",it_number);
  ierr = IGAWriteVec(user->iga,U,filename);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "FormInitialCondition"
PetscErrorCode FormInitialCondition(IGA iga,Vec U)
{
  PetscErrorCode ierr;
  PetscFunctionBegin;
  Vec          Ul;
  PetscScalar *u;
  PetscInt     n;
  ierr = IGAGetLocalVec(iga,&Ul);CHKERRQ(ierr);
  ierr = VecGetArray(Ul,&u);CHKERRQ(ierr);
  ierr = VecGetSize(Ul,&n);CHKERRQ(ierr);
  ierr = PetscMemcpy(u,iga->propertyA,n*sizeof(PetscScalar));CHKERRQ(ierr);
  ierr = VecRestoreArray(Ul,&u);CHKERRQ(ierr);
  ierr = IGALocalToGlobal(iga,Ul,U,INSERT_VALUES);CHKERRQ(ierr);
  ierr = VecDestroy(&Ul);CHKERRQ(ierr);
  PetscFunctionReturn(0); 
}

#undef __FUNCT__
#define __FUNCT__ "KISSAdaptivity"
PetscErrorCode KISSAdaptivity(TS ts,PetscReal t,Vec X,Vec Xdot, PetscReal *nextdt,PetscBool *ok,void *ctx)
{
  PetscErrorCode       ierr;
  PetscReal            dt;
  SNES                 snes;
  SNESConvergedReason  snesreason;
  PetscInt             sit;
  ierr = TSGetTimeStep(ts,&dt);CHKERRQ(ierr);
  ierr = TSGetSNES(ts,&snes);CHKERRQ(ierr);
  ierr = SNESGetConvergedReason(snes,&snesreason);CHKERRQ(ierr);
  ierr = SNESGetIterationNumber(snes,&sit);CHKERRQ(ierr);

  // default
  *ok = PETSC_TRUE;
  *nextdt = dt;
  
  // Check the worst-case Fourier number
  AppCtx     *user = (AppCtx *)ctx;
  MPI_Comm    comm;
  PetscScalar kappa_max,Fo;
  ierr = IGAGetComm(user->iga,&comm);CHKERRQ(ierr);
  ierr = MPI_Allreduce(&(user->kappa_max),&kappa_max,1,MPIU_REAL,MPIU_MAX,comm);CHKERRQ(ierr);
  user->kappa_max = 0;
  Fo = kappa_max*dt/user->h2;

  if (Fo > TOL_FO) { // Fo number too large
    *ok = PETSC_FALSE;
    *nextdt = TOL_FO*user->h2/kappa_max;
    *nextdt = PetscMin(0.1*dt,*nextdt);
    PetscFunctionReturn(0);
  }
  if (snesreason < 0) { // SNES failed
    *ok = PETSC_FALSE;
    *nextdt = 0.1*dt;
    PetscFunctionReturn(0);
  }
  if (sit < 3) { // too easy, increase dt
    *ok = PETSC_TRUE;
    *nextdt = 1.1*dt;
    PetscFunctionReturn(0);
  }
  if (sit > 5) { // too hard, decrease dt
    *ok = PETSC_TRUE;
    *nextdt = 0.5*dt;
    PetscFunctionReturn(0);
  }
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "main"
int main(int argc, char *argv[]) {

  PetscErrorCode  ierr;
  ierr = PetscInitialize(&argc,&argv,0,0);CHKERRQ(ierr);

  AppCtx user;
  user.alpha     = 5.0/3.0;
  user.gamma     = 0.5;
  user.Cf        = 0.009;
  user.m         = 1.0+user.alpha/user.gamma;
  user.kappa_max = 0;
  PetscScalar L = 4e3, dx = 10;
  PetscInt p = 1,k = 0,N = (PetscInt)(L/dx);
  PetscBool output = PETSC_TRUE;
  user.h2 = dx*dx;

  IGA iga;
  ierr = IGACreate(PETSC_COMM_WORLD,&iga);CHKERRQ(ierr);
  ierr = IGASetDim(iga,2);CHKERRQ(ierr);
  ierr = IGASetDof(iga,1);CHKERRQ(ierr);

  IGAAxis axis0;
  ierr = IGAGetAxis(iga,0,&axis0);CHKERRQ(ierr);
  ierr = IGAAxisSetDegree(axis0,p);CHKERRQ(ierr);
  ierr = IGAAxisInitUniform(axis0,N,-0.5*L,0.5*L,k);CHKERRQ(ierr);
  IGAAxis axis;
  ierr = IGAGetAxis(iga,1,&axis);CHKERRQ(ierr);
  ierr = IGAAxisCopy(axis0,axis);CHKERRQ(ierr);
  ierr = IGASetFromOptions(iga);CHKERRQ(ierr);
  ierr = IGASetUp(iga);CHKERRQ(ierr);
  ierr = IGAWrite(iga,"iga.dat");CHKERRQ(ierr);
  user.iga = iga;
  
  ierr = IGASetUserIFunction(iga,Residual,&user);CHKERRQ(ierr);
  ierr = IGASetUserIJacobian(iga,Jacobian,&user);CHKERRQ(ierr);

  TS ts;
  ierr = IGACreateTS(iga,&ts);CHKERRQ(ierr);
  ierr = TSSetDuration(ts,1000000,40.*60);CHKERRQ(ierr);
  ierr = TSSetTimeStep(ts,1.0e-2);CHKERRQ(ierr);
  ierr = TSSetType(ts,TSALPHA);CHKERRQ(ierr);
  ierr = TSAlphaSetRadius(ts,0.5);CHKERRQ(ierr);
  ierr = TSAlphaSetAdapt(ts,KISSAdaptivity,&user);CHKERRQ(ierr); 
  if (output)  {ierr = TSMonitorSet(ts,OutputMonitor,&user,NULL);CHKERRQ(ierr);}
  ierr = TSSetFromOptions(ts);CHKERRQ(ierr);

  Vec U;
  ierr = IGACreateVec(iga,&U);CHKERRQ(ierr);
  ierr = FormInitialCondition(iga,U);CHKERRQ(ierr);
  ierr = TSSolve(ts,U);CHKERRQ(ierr);

  ierr = VecDestroy(&U);CHKERRQ(ierr);
  ierr = TSDestroy(&ts);CHKERRQ(ierr);
  ierr = IGADestroy(&iga);CHKERRQ(ierr);

  ierr = PetscFinalize();CHKERRQ(ierr);
  return 0;
}
