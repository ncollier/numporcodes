import numpy as np
import pylab as plt
from igakit.cad import grid
from igakit.io import PetIGA
from igakit.nurbs import NURBS

def LaplacianSmoothing(A,it):
    for i in range(it):
        B = np.zeros(A.shape)
        B[:-1,:] += A[1:,:]
        B[1:,:]  += A[:-1,:]
        B[:,:-1] += A[:,1:]
        B[:,1:]  += A[:,:-1]
        B[1:-1,1:-1] /= 4.
        B[1:-1,0] /= 3.
        B[1:-1,-1] /= 3.
        B[0,1:-1] /= 3.
        B[-1,1:-1] /= 3.
        B[0,0] /= 2.
        B[-1,0] /= 2.
        B[0,-1] /= 2.
        B[-1,-1] /= 2.
        A = np.copy(B)
    return A

data = np.fromfile('./n37w119/floatn37w119_1.flt',dtype='f4').reshape((3612,-1))
clip = np.copy(data[1200:2000,1600:2400])[::2,::2]

plt.subplot(121)
cb=plt.imshow(clip)
plt.colorbar(cb)
clip = LaplacianSmoothing(clip,100)
x0 = clip.min()
dx = clip.max()-clip.min()
clip = (clip-x0)/dx *10.
plt.subplot(122)
cb=plt.imshow(clip)
plt.colorbar(cb)
plt.show()

z = grid((clip.shape[0]-1,clip.shape[1]-1),degree=1,continuity=0,limits=[-2e3,2e3])
nrbs = NURBS(z.knots,z.control,fields=clip)
PetIGA().write('../elevation.dat',nrbs,control=False,fields=True,nsd=2)
