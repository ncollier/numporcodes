from progress import ProgressBar
from igakit.nurbs import NURBS
from igakit.io import PetIGA
import pylab as plt
import numpy as np

def ParseLog(fname="../dsw.log"):
    t = []
    dts = []
    log = file(fname).readlines()
    for line in log:
        line = line.split()
        if len(line) < 2: continue
        if line[1] == "TS":
            t.append(float(line[-1]))
            dts.append(float(line[3]))
    t = np.asarray(t)
    dts = np.asarray(dts)
    return t,dts

def ComputeU(t0,ts,iga):
    i = ts.searchsorted(t0)
    u0 = PetIGA().read_vec("./data/dsw%d.dat" % (i  ),iga)
    uf = PetIGA().read_vec("./data/dsw%d.dat" % (i+1),iga)
    a = (t0-ts[i])/(ts[i+1]-ts[i])
    return (1.-a)*u0+a*uf

def Plot(t,ts,xs,iga,Z,count):
    fig = plt.figure(figsize=(6,6),tight_layout=True)
    U = ComputeU(t,ts,iga)
    H = (U-Z)*100.
    cb = plt.imshow(H.clip(0),vmax=25.,cmap='Blues')
    plt.colorbar(cb)
    plt.contour(Z,40,cmap='terrain',alpha=0.9)
    xt = (xs/4000.+0.5)*H.shape[0]
    #plt.plot(xt[:,1],xt[:,0],'or',markersize=3,mew=0)
    fig.axes[0].set_xticklabels([])
    fig.axes[0].set_yticklabels([])
    fig.axes[0].set_xticks([])
    fig.axes[0].set_yticks([])
    plt.title('Water Depth (cm) at t = %d' % (int(t)))
    fig.savefig('time%d.pdf' % count)
    plt.close(fig)


alpha = 5./3.
gamma = 0.5
Cf    = 0.009/10.
Ns    = 7
dt    = 5.*60

iga = PetIGA().read("../iga.dat")
Z   = PetIGA().read_vec("./data/dsw0.dat",iga); Z[0,0] = -2.5
ts,dts = ParseLog()

xs  = np.zeros((Ns,2))
xs[0,:] = [1730, 670]
xs[1,:] = [1278, 732]
xs[2,:] = [ 878, 800]
xs[3,:] = [ 411, 790]
xs[4,:] = [-100, 744]
xs[5,:] = [-348, 619]
xs[6,:] = [-355, 46 ]

ind = np.arange(Ns)
N   = ts.shape[0]
pr  = ProgressBar(N-1)
ct  = 0.
cnt = 0

for i in range(N):
    pr.animate(i)
    U     = PetIGA().read_vec("./data/dsw%d.dat" % i,iga)
    xt,yt = np.copy(xs[:,0]),np.copy(xs[:,1])
    u     = iga.evaluate(fields=U,u=xt,v=yt)[ind,ind]
    u_x   = iga.gradient(fields=U,u=xt,v=yt)[ind,ind,:]
    z     = iga.evaluate(fields=Z,u=xt,v=yt)[ind,ind]
    h     = u-z
    ka    = (h.clip(1e-15))**alpha * np.linalg.norm(u_x)**(gamma-1.)/Cf
    flux  = u_x*ka[...,None]
    xs   -= flux*dts[i]
    if ts[i] > ct:
        Plot(ts[i],ts,xs,iga,Z,cnt)
        ct  += dt
        cnt += 1
