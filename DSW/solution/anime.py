import numpy as np
import pylab as plt
from igakit.cad import grid
from igakit.io import PetIGA
from igakit.nurbs import NURBS
import glob,sys,os
from progress import ProgressBar

def FindPoint(h):
    for i in range(h.shape[0]):
        if h[i] < 0.: return i
    return h.shape[0]-1

def FindExtent(pnt,n,u,z,iga,hw=100.,res=10):
    L = np.zeros(2); L[0] = n[1]; L[1] = -n[0]
    xtry = pnt+np.outer(np.linspace(0,1,res),L)*hw
    uu = iga.evaluate(fields=u,u=xtry[:,1].copy(),v=xtry[:,0].copy()).diagonal()
    du = iga.gradient(fields=u,u=xtry[:,1].copy(),v=xtry[:,0].copy()).diagonal().T
    zz = iga.evaluate(fields=z,u=xtry[:,1].copy(),v=xtry[:,0].copy()).diagonal()
    hh = uu-zz
    ind = FindPoint(hh)
    magdu = np.sqrt(du[...,0]*du[...,0]+du[...,1]*du[...,1])
    ka    = ((uu-zz).clip(1e-15))**alpha * magdu**(gamma-1.)/Cf
    flux  = du*ka[...,None]
    delta = np.linalg.norm(xtry[1,:]-xtry[0,:])
    Q     = np.dot(flux[:ind],n).sum()*delta
    A     = hh[:ind].sum()*delta
    return np.abs(Q),A

def Limits(datafiles,z):
    print "Finding water depth limits..."
    hmin = []
    hmax = []
    for i in range(len(datafiles)):
        fname = datafiles[i]
        u = PetIGA().read_vec(fname,iga)
        h = u-z
        hmin.append(h.min())
        hmax.append(h.max())
    print "Limits found: 0, %.3f" % max(hmax)
    return min(min(hmin),0),max(hmax)

def ComputeU(t0,ts,iga):
    i = ts.searchsorted(t0)
    u0 = PetIGA().read_vec("./data/dsw%d.dat" % (i  ),iga)
    uf = PetIGA().read_vec("./data/dsw%d.dat" % (i+1),iga)
    a = (t0-ts[i])/(ts[i+1]-ts[i])
    return (1.-a)*u0+a*uf

def AdvectSensors(t0,tf,ts,dts,xs,iga):
    a = ts.searchsorted(t0)
    b = ts.searchsorted(tf)
    ind = np.arange(xs.shape[0])
    if b-a == 0: return xs
    p = ProgressBar(b-a)
    for i in range(a,b+1):
        p.animate(i-a)
        if i == a:
            u  = ComputeU(t0,ts,iga)
            dt = ts[a+1]-t0
        elif i == b:
            u = PetIGA().read_vec("./data/dsw%d.dat" % i,iga)
            dt = tf-ts[b]
        else:
            u = PetIGA().read_vec("./data/dsw%d.dat" % i,iga)
            dt = dts[i]
        n = iga.gradient(fields=u,u=xs[0,0],v=xs[1,1])
        Q1,A1 = FindExtent(xs[0,:], n,u,z,iga)
        Q2,A2 = FindExtent(xs[0,:],-n,u,z,iga)
        if(A1+A2<1e-12): return xs
        xs += -n*(Q1+Q2)/(A1+A2)*dt
    print " "
    return xs

os.system("rm time*.png")
Ns = 7
xs = np.zeros((Ns,2)) # x,y position
xs[0,:] = [ 670, 1730]
xs[1,:] = [ 732, 1278]
xs[2,:] = [ 800, 878]
xs[3,:] = [ 790, 411]
xs[4,:] = [ 744,-100]
xs[5,:] = [ 619,-348]
xs[6,:] = [  46,-355]

Ns = 1
xs[0,:] = [ 619,-348]
old = np.asarray([ 619,-348])

# Parse logfile
t = []
dts = []
log = file('../dsw.log').readlines()
for line in log:
    line = line.split()
    if len(line) < 2: continue
    if line[1] == "TS":
        t.append(float(line[-1]))
        dts.append(float(line[3]))
t = np.asarray(t)
dts = np.asarray(dts)

# initialize DSW stuff
iga = PetIGA().read("../iga.dat")
z   = PetIGA().read_vec("./data/dsw0.dat",iga)
alpha = 5./3.
gamma = 0.5
Cf    = 0.009
datafiles = glob.glob("./data/dsw*.dat")
hmin,hmax = 0,0.25 # = Limits(datafiles,z)
z[0,0] = -2.5 # keeps blue out of the terrain colormap

# render plots
print "Rendering plots..."
tfinal = 2000.
dt = 100.
nsteps = int(tfinal/dt)+1
for i in range(nsteps):
    time = i*dt
    print "\ttime = %.2f" % time
    fig = plt.figure(figsize=(6,6),tight_layout=True)

    # plot water height
    u = ComputeU(time,t,iga)
    h = (u-z)*100.
    cb = plt.imshow(h.clip(0),vmax=100*hmax,cmap='Blues')
    plt.colorbar(cb)

    # plot terrain
    CS = plt.contour(z,40,cmap='terrain',alpha=0.9)

    # update and plot sensors
    xs=AdvectSensors(max(0,(i-1)*dt),time,t,dts,xs,iga)
    print "\nSensor: (%f %f) disp = %f" % (xs[0,0],xs[0,1],np.linalg.norm(xs[0,:]-old))
    for j in range(Ns):
        xt = ((xs[j,:]/4000.)+0.5)
        xt *= h.shape[0]
        plt.plot(xt[0],xt[1],'or',markersize=3,mew=0)

    fig.axes[0].set_xticklabels([])
    fig.axes[0].set_yticklabels([])
    fig.axes[0].set_xticks([])
    fig.axes[0].set_yticks([])
    plt.title('Water Depth (cm)')
    fig.savefig('time%d.png' % i)
    plt.close(fig)
