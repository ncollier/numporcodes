import numpy as np
import pylab as plt
from igakit.io import PetIGA
import glob,sys

def FindPoint(h):
    for i in range(h.shape[0]):
        if h[i] < 0.: return i
    return h.shape[0]-1

def FindExtent(pnt,n,hw=100.,res=1000):
    L = np.zeros(2); L[0] = n[1]; L[1] = -n[0]
    xtry = pnt+np.outer(np.linspace(0,1,res),L)*hw
    plt.plot(xtry[:,0],xtry[:,1],'-r')
    uu = iga.evaluate(fields=u,u=xtry[:,1].copy(),v=xtry[:,0].copy()).diagonal()
    du = iga.gradient(fields=u,u=xtry[:,1].copy(),v=xtry[:,0].copy()).diagonal().T
    zz = iga.evaluate(fields=z,u=xtry[:,1].copy(),v=xtry[:,0].copy()).diagonal()
    hh = uu-zz
    plt.subplot(122)
    plt.plot(xtry[:,0],hh,'-b')
    ind = FindPoint(hh)
    plt.plot(xtry[ind,0],hh[ind],'ob')
    plt.subplot(121)
    plt.plot(xtry[ind,0],xtry[ind,1],'or')
    magdu = np.sqrt(du[...,0]*du[...,0]+du[...,1]*du[...,1])
    ka    = ((uu-zz).clip(1e-15))**alpha * magdu**(gamma-1.)/Cf
    flux  = du*ka[...,None]
    print flux[0,:]
    delta = np.linalg.norm(xtry[1,:]-xtry[0,:])
    Q     = np.dot(flux[:ind],n).sum()*delta
    A     = hh[:ind].sum()*delta
    return np.abs(Q),A

i = 0
try:
    i = int(sys.argv[1])
except:
    pass

iga   = PetIGA().read("../iga.dat")
u     = PetIGA().read_vec("./data/dsw%d.dat" % i,iga)
z     = PetIGA().read_vec("./data/dsw0.dat",iga)
alpha = 5./3.
gamma = 0.5
Cf    = 0.009
x     = iga.breaks(0)[:-1]+10.
uu    = iga.evaluate(fields=u,u=x,v=x)
du    = iga.gradient(fields=u,u=x,v=x)
zz    = iga.evaluate(fields=z,u=x,v=x)
dz    = iga.gradient(fields=z,u=x,v=x)
magdu = np.sqrt(du[...,0]*du[...,0]+du[...,1]*du[...,1])
ka    = ((uu-zz).clip(1e-15))**alpha * magdu**(gamma-1.)/Cf
vel   = du*ka[...,None]
hh    = uu-zz

plt.figure(figsize=(20,7))
plt.subplot(121)
xx,yy = plt.meshgrid(x,x)
cb = plt.contourf(xx,yy,hh.clip(0),40,cmap='Blues')
plt.colorbar(cb)

xs = [ 619,-348]
xs = [ 592,-316]
du = iga.gradient(fields=u,u=xs[0],v=xs[1]); du /= np.linalg.norm(du)
plt.plot(xs[0],xs[1],'or')
plt.xlim((400,800))
plt.ylim((-600,-100))

Q1,A1 = FindExtent(xs, du)
Q2,A2 = FindExtent(xs,-du)
print (Q1+Q2)/(A1+A2)

plt.show()
